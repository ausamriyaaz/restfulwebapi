package com.ausam;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

public class ConnectionPool {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String JDBC_DB_URL = "jdbc:mysql://localhost:3306/people";

    static final String JDBC_USER = "root";
    static final String JDBC_PASS = "root";

    private static GenericObjectPool gPool = null;

    @SuppressWarnings("unused")
    public DataSource setUpPool() throws Exception {
        Class.forName(JDBC_DRIVER);

        gPool = new GenericObjectPool();
        gPool.setMaxActive(20);

        ConnectionFactory cf = new DriverManagerConnectionFactory(JDBC_DB_URL, JDBC_USER, JDBC_PASS);

        PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, gPool, null, null, false, true);
        return new PoolingDataSource(gPool);
    }

    public GenericObjectPool getConnectionPool() {
        return gPool;
    }


    private void printDbStatus() {
        System.out.println("Max.: " + getConnectionPool().getMaxActive() + "; Active: " + getConnectionPool().getNumActive() + "; Idle: " + getConnectionPool().getNumIdle());
    }

    public static void addToDb(Person person) {
        ResultSet rsObj = null;
        Connection connObj = null;
        PreparedStatement pstmtObj = null;
        ConnectionPool jdbcObj = new ConnectionPool();
        try {
            DataSource dataSource = jdbcObj.setUpPool();
            jdbcObj.printDbStatus();

            System.out.println("New Connection Object For Db Transaction");
            connObj = dataSource.getConnection();
            jdbcObj.printDbStatus();

            PreparedStatement preparedStatement = connObj.prepareStatement("INSERT INTO people.person (id ,name,age) VALUES (NULL , ?,?)");
            preparedStatement.setString(1,  person.getName());
            preparedStatement.setInt(2,person.getAge());
            preparedStatement.executeUpdate();

            System.out.println("Releasing Connection Object To Pool");
        } catch(Exception sqlException) {
            sqlException.printStackTrace();
        } finally {
            try {
                if(rsObj != null) {
                    rsObj.close();
                }
                if(pstmtObj != null) {
                    pstmtObj.close();
                }
                if(connObj != null) {
                    connObj.close();
                }
            } catch(Exception sqlException) {
                sqlException.printStackTrace();
            }
        }
        jdbcObj.printDbStatus();
    }
}