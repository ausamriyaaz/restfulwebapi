package com.ausam;

import org.apache.log4j.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.PipedReader;
import java.io.StringReader;

@Path("/hello")
public class HelloWorld {
    static Logger log = Logger.getLogger(HelloWorld.class.getName());

    private static Document convertStringToXMLDocument(String xmlString) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();

            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } catch (Exception e) {
            log.error(e);

        }
        return null;
    }

    @GET
    @Path("/get")
    public String getMessage() {
        return "test!";
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML})
    @Produces({MediaType.TEXT_PLAIN})
    @Path("/post")
    public Response postMessage(String xml) throws Exception {
        String json = "";


        if (validateXML(xml) == true) {

            json = converXMLToJson(xml);
            return Response.status(400).entity(json).build();
        } else {
            return Response.status(400).entity("invalid xml").build();
        }


//    ------------------------------------------------------------------------------------------------------------------------
//        System.out.println(xml);
//        final String xmlStr = xml;
//
//
//
//        Document document = convertStringToXMLDocument(xmlStr);
//
//        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
//
//        Source schemaFile = new StreamSource(new File("person.xsd"));
//        Schema schema = factory.newSchema(schemaFile);
//        XsdErrorHandler xsdErrorHandler = new XsdErrorHandler();
//
//        Validator validator = schema.newValidator();
//        validator.setErrorHandler(xsdErrorHandler);
//        XmlMapper xmlMapper = new XmlMapper();
//        Person person1 = xmlMapper.readValue(xml, Person.class);
//        ObjectMapper mapper = new ObjectMapper();
//        String json = mapper.writeValueAsString(person1);
//
//        try {
//
//            validator.validate(new DOMSource(document));
//            System.out.println("file is valid");
//            ConnectionPool.addToDb(person1);
////            person.setName(person1.getName());
////            person.setAge(person1.getAge());
////            jdbcPersonDAO.insert(person);
////            jdbcPersonDAO.closeConnection();
//        } catch (SAXException e) {
//            log.error(e);
//            System.out.println("file is invalid");
//            return Response.status(200).entity("invalid xml").build();
//
//        }


//     ---------------------------------------------------------------------------------------------------------------------


    }

    @POST
    @Consumes({MediaType.APPLICATION_XML})
    @Produces({MediaType.TEXT_PLAIN})
    @Path("/insertToDb")
    public Response getXml(String xml) throws Exception {
        String json = "";
        if (validateXML(xml) == true) {

            XmlMapper xmlMapper = new XmlMapper();
            Person person1 = xmlMapper.readValue(xml, Person.class);


            ConnectionPool.addToDb(person1);
            return Response.status(400).entity("added values to the database").build();
        } else {
            return Response.status(200).entity("invalid xml").build();
        }
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML})
    @Produces({MediaType.TEXT_PLAIN})
    @Path("/json")
    public Response getJson(String xml) throws Exception {
        return Response.status(200).entity("adf").build();
    }


    public Boolean validateXML(String xml) throws Exception {
        String json = "";
        boolean valid = true;
        try {
            final String xmlStr = xml;

            Document document = convertStringToXMLDocument(xmlStr);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(true);
            factory.setValidating(false);
            SAXParser saxParser = factory.newSAXParser();


            XMLReader xmlReader = SAXParserFactory.newInstance()
                    .newSAXParser().getXMLReader();

            InputSource source = new InputSource(new StringReader(xmlStr));

            XsdErrorHandler2 handler = new XsdErrorHandler2();

            saxParser.parse(source, handler);

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Source schemaFile = new StreamSource(new File("person.xsd"));
            Schema schema = schemaFactory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.setErrorHandler(handler);
            validator.validate(new DOMSource(document));


        } catch (SAXException se) {
            valid = false;
            System.out.println(se.getMessage());

        } catch (IOException ioe) {
            valid = false;
        }
        return valid;
    }

    public String converXMLToJson(String xml) throws IOException {
        String json;
        XmlMapper xmlMapper = new XmlMapper();
        Person person1 = xmlMapper.readValue(xml, Person.class);
        ObjectMapper mapper = new ObjectMapper();
        json = mapper.writeValueAsString(person1);
        return json;

    }
}