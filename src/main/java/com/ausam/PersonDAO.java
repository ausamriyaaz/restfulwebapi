package com.ausam;


import java.util.List;

public interface PersonDAO {

    public void insert(PersonDB person);
    public List<PersonDB> select();

}
